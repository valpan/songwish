import SongCard from '@/components/SongCard.vue'
import { mount } from '@vue/test-utils'

describe('SongCard', () => {
  test('Check if card is rendered', () => {
    const wrapper = mount(SongCard)
    expect(wrapper.find('.card').isVisible()).toBe(true)
  })

  test('When a card is started by default (GuestView), the  Remove Button isnt rendered', () => {
    const wrapper = mount(SongCard)
    expect(wrapper.find('button').isVisible()).toBe(false)
  })

  test('if a card is loaded in DJView, then the Remove button on it should be visible', async () => {
    const wrapper = mount(SongCard, {
      propsData: {
        isDJView: true
      }
    })
    expect(wrapper.find('button').isVisible()).toBe(true)
  })

  test('if button is clicked, randomNumber should be between 1 and 10', async () => {
    const wrapper = mount(SongCard)
    wrapper.find('button').trigger('click')
    await wrapper.vm.$nextTick()
    const randomNumber = parseInt(wrapper.find('span').element.textContent)
    expect(randomNumber).toBeGreaterThanOrEqual(1)
    expect(randomNumber).toBeLessThanOrEqual(10)
  })
})
