import Vue from 'vue'
import Vuex from 'vuex'
import SongService from '@/services/SongService.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    djPassword: '',
    guestPassword: '',
    songs: [],
    songsTotal: 0,
    wishedSongs: [],
    wishedSongsTotal: 0,
    selectedSongId: 0
  },
  mutations: {
    SET_DJ_PASSWORD(state, password) {
      state.djPassword = password
    },
    SET_GUEST_PASSWORD(state, password) {
      state.guestPassword = password
    },

    ADD_WISHED_SONG(state, song) {
      state.wishedSongs.push(song)
    },
    SET_SONGS(state, songs) {
      state.songs = songs
    }, //will be needed when there is pagination, maybe
    SET_SONGS_TOTAL(state, songsTotal) {
      state.songsTotal = songsTotal
    },
    SET_WISHED_SONGS(state, wishedSongs) {
      state.wishedSongs = wishedSongs
    },
    SET_WISHED_SONGS_TOTAL(state, wishedSongsTotal) {
      state.wishedSongsTotal = wishedSongsTotal
    },
    SET_SELECTED_SONG(state, songId) {
      state.selectedSongId = songId
    }
  },
  actions: {
    // login({ commit, state }, credentials) {},
    wishSong({ commit, state }) {
      const song = state.songs.find(
        element => element.id === state.selectedSongId
      )
      return SongService.postWishedSong(song)
        .then(() => {
          commit('ADD_WISHED_SONG', song)
          commit('SET_SELECTED_SONG', 0)
        })
        .catch(error => {
          console.log('ERROR ON ADDING SONG ', error)
        })
    },
    fetchSongs({ commit }) {
      return SongService.getSongs()
        .then(response => {
          commit('SET_SONGS', response.data)
          //Will have to be fixed maybe, when the DB is changed
          commit('SET_SONGS_TOTAL', response.headers['x-total-count'])
        })
        .catch(error => {
          console.log('ERROR BY FETCHING SONGS ', error)
        })
    },
    fetchWishedSongs({ commit }) {
      return SongService.getWishedSongs()
        .then(response => {
          commit('SET_WISHED_SONGS', response.data)
          //Will have to be fixed maybe, when the DB is changed
          commit('SET_WISHED_SONGS_TOTAL', response.headers['x-total-count'])
        })
        .catch(error => {
          console.log('ERROR BY FETCHING WISHED SONGS ', error)
        })
    },
    selectSong({ commit }, songId) {
      commit('SET_SELECTED_SONG', songId)
    },
    removeWishedSong({ dispatch }, song) {
      return SongService.deleteWishedSong(song.id)
        .then(() => {
          dispatch('fetchWishedSongs')
        })
        .catch(err => {
          console.log('DELETE FAIL: ' + err)
        })
    }
  },
  getters: {
    isSongClicked: state => songId => {
      //Needs improvement to separate DJ from User Clicks, maybe
      return state.selectedSongId === songId
    },
    isSongWished: state => song => {
      return state.wishedSongs.filter(e => e.id === song.id).length > 0
    },
    isSelectedSongWished: state => {
      return (
        state.wishedSongs.filter(e => e.id === state.selectedSongId).length > 0
      )
    },
    isSongSelected: state => {
      return state.selectedSongId != 0
    }
  },
  modules: {}
})
