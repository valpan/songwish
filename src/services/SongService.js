import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 10000
})

export default {
  getAppPasswords() {
    return apiClient.get('/appPasswords')
  },
  getSongs() {
    return apiClient.get('/songs')
  },
  getSong(id) {
    return apiClient.get('/songs/' + id)
  },
  getWishedSongs() {
    return apiClient.get('/wishedSongs')
  },
  postWishedSong(song) {
    return apiClient.post('/wishedSongs', song)
  },
  deleteWishedSong(songid) {
    return apiClient.delete('/wishedSongs/' + songid)
  }
}
