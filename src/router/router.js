import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import GuestView from '@/views/GuestView.vue'
import DJView from '@/views/DJView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/guest-view',
    name: 'guest-view',
    component: GuestView
  },
  {
    path: '/dj-view',
    name: 'dj-view',
    component: DJView
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
